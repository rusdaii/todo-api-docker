const { Todo } = require('../models');
class TodoController {
  static findAll = async (req, res, next) => {
    try {
      const todos = await Todo.findAll(req.query);
      res.status(200).json(todos);
    } catch (err) {
      next(err);
    }
  };

  static findOne = async (req, res, next) => {
    try {
      const todo = await Todo.findByPk(req.params.id);

      if (!todo) {
        throw { name: 'ErrorNotFound' };
      }
      return res.status(200).json({ message: 'Succes', todo });
    } catch (err) {
      next(err);
    }
  };

  static create = async (req, res, next) => {
    try {
      await Todo.create(req.body);

      res.status(201).json({ message: 'Todo created successfully' });
    } catch (err) {
      next(err);
    }
  };

  static update = async (req, res, next) => {
    try {
      const [updatedCount, [updatedTodo]] = await Todo.update(req.body, {
        where: { id: req.params.id },
        returning: true,
      });

      if (updatedCount === 0 || !updatedTodo) {
        throw { name: 'ErrorNotFound' };
      }

      res
        .status(200)
        .json({ message: 'Todo updated successfully', updatedTodo });
    } catch (err) {
      next(err);
    }
  };

  static destroy = async (req, res, next) => {
    try {
      const todoId = req.params.id;

      if (!todoId) {
        throw { name: 'ErrorNotFound' };
      }

      const todo = await Todo.findByPk(todoId);

      if (!todo) {
        throw { name: 'ErrorNotFound' };
      }

      await todo.destroy();
      res.status(200).json({ message: 'Todo deleted successfully' });
    } catch (err) {
      next(err);
    }
  };
}

module.exports = TodoController;
