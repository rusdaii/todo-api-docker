require('dotenv').config();

const config = {
  development: {
    username: process.env.POSTGRES_USER,
    password: process.env.POSGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    host: process.env.POSTGRES_HOST,
    dialect: 'postgres',
    port: process.env.PGPORT,
  },
  test: {
    username: process.env.POSTGRES_USER,
    password: process.env.POSGRES_PASSWORD,
    database: process.env.POSTGRES_DB_TEST,
    host: process.env.POSTGRES_HOST,
    dialect: 'postgres',
    port: process.env.PGPORT,
  },
};

module.exports = config;
