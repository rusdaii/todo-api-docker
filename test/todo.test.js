const app = require('../app');
const request = require('supertest');
const { sequelize } = require('../models');
const { queryInterface } = sequelize;

beforeAll((done) => {
  queryInterface
    .bulkInsert(
      'Todos',
      [
        {
          title: 'Do Homework',
          status: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: 'Go To Gym',
          status: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: 'Watch Movie',
          status: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    )
    .then((_) => {
      done();
    })
    .catch((err) => {
      console.log(err);
      done(err);
    });
});

afterAll((done) => {
  queryInterface
    .bulkDelete('Todos', null, {})
    .then((_) => {
      done();
    })
    .catch((err) => {
      console.log(err);
      done(err);
    });
});

describe('Todo Controller', () => {
  describe('findAll', () => {
    it('should retrieve and return all todo', async () => {
      const response = await request(app)
        .get('/todo')
        .expect('Content-Type', /json/)
        .expect(200);

      expect(Array.isArray(response.body)).toBe(true);

      expect(response.body.length).toBeGreaterThan(0);

      const firstTodo = response.body[0];
      expect(firstTodo).toHaveProperty('id');
      expect(firstTodo).toHaveProperty('title');
      expect(firstTodo).toHaveProperty('status');

      expect(response.status).toBe(200);
    });
  });

  describe('findOne', () => {
    it('should retrieve and return a single task by id', async () => {
      const existingTodoId = 1; // Ganti dengan ID task yang ada

      const response = await request(app)
        .get(`/todo/${existingTodoId}`)
        .expect('Content-Type', /json/)
        .expect(200);

      // struktur data dalam respons
      const todo = response.body.todo;
      expect(todo).toHaveProperty('id');
      expect(todo).toHaveProperty('title');
      expect(todo).toHaveProperty('status');

      expect(response.status).toBe(200);
    });

    it('should handle not found error and call next', async () => {
      const nonExistingTodoId = 9999; // ID yang tidak ada

      const response = await request(app)
        .get(`/todo/${nonExistingTodoId}`)
        .expect(404);

      expect(response.status).toBe(404);
    });
  });
  describe('create', () => {
    it('should create a new todo', async () => {
      const newTodo = {
        title: 'New Todo',
        status: true,
      };

      const response = await request(app)
        .post('/todo')
        .send(newTodo)
        .expect('Content-Type', /json/)
        .expect(201);

      expect(response.body.message).toBe('Todo created successfully');

      expect(response.status).toBe(201);
    });

    it('should handle errors and call next', async () => {
      const invalidTodo = {
        status: 'invalid', // status harus boolean
      };

      const response = await request(app)
        .post('/todo')
        .send(invalidTodo)
        .expect(500);

      expect(response.status).toBe(500);
    });
  });

  describe('update', () => {
    it('should update an existing todo', async () => {
      const existingTodoId = 1; // Ganti dengan ID task yang ada

      const updatedData = {
        title: 'Updated Todo',
        status: false,
      };

      const response = await request(app)
        .put(`/todo/${existingTodoId}`)
        .send(updatedData)
        .expect('Content-Type', /json/)
        .expect(200);

      expect(response.body.message).toBe('Todo updated successfully');

      expect(response.status).toBe(200);
    });

    it('should handle not found error and call next', async () => {
      const nonExistingTodoId = 9999; // ID yang tidak ada

      const updatedData = {
        title: 'Updated Todo',
        status: false,
      };

      const response = await request(app)
        .put(`/todo/${nonExistingTodoId}`)
        .send(updatedData)
        .expect(404);

      expect(response.status).toBe(404);
    });
  });

  describe('destroy', () => {
    it('should delete an existing task', async () => {
      const existingTodoId = 1; // Ganti dengan ID task yang ada

      const response = await request(app)
        .delete(`/todo/${existingTodoId}`)
        .expect('Content-Type', /json/)
        .expect(200);

      expect(response.body.message).toBe('Todo deleted successfully');

      expect(response.status).toBe(200);
    });

    it('should handle not found error and call next', async () => {
      const nonExistingTodoId = 9999; // ID yang tidak ada

      const response = await request(app)
        .delete(`/todo/${nonExistingTodoId}`)
        .expect(404);

      expect(response.status).toBe(404);
    });
  });
});
