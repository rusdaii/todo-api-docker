FROM node:18-alpine3.18

WORKDIR /todoapi

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 8090

CMD [ "node", "app.js" ]