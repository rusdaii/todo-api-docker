require('dotenv').config();
const express = require('express');
const app = express();
const port = 8090;
const router = require('./routes');
const errorHandler = require('./middlewares/errorHandler');

app.use(express.json());
app.use(router);
app.use(errorHandler);

if (process.env.NODE_ENV != 'test') {
  app.listen(port, () => {
    console.log(`Server is live on port ${port}`);
  });
}

module.exports = app;
